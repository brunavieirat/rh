﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Senai.RH.Model
{
    class Funcionario
    {

        private long id;

        public long ID
        {
            get { return id; }
            set { id = value; }
        }

        private string nome;

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }


        private string rg;

        public string RG
        {
            get { return rg; }
            set { rg = value; }
        }

        private string cpf;

        public string CPF
        {
            get { return cpf; }
            set { cpf = value; }
        }


        private string telefone;

        public string Telefone
        {
            get { return telefone; }
            set { telefone = value; }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }


        public Funcionario()
        {

        }

        public Funcionario(long id, string nome, string rg, string cpf, string telefone, string email)
        {
            this.id = id;
            this.nome = nome;
            this.rg = rg;
            this.cpf = cpf;
            this.telefone = telefone;
            this.email = email;
        }


        public override string ToString()
        {
            return this.nome;
        }
    }
}
