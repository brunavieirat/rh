﻿using Senai.RH.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RH.Senai.RH.Dao
{
    class FuncionarioDao : IDao<Funcionario>
    {
        //atributos
        //con~xão com o bd
        private SqlConnection connection;

        //instrução sql
        private string sql = null;

        //mensagem do messagebox
        private string msg = null;

        //titulo do messagebox
        private string titulo = null;


        public FuncionarioDao()
        {
            //cria uma conxexão com o banco de dados
            connection = new ConnectionFactory().GetConnection();
        }


        public  List<Funcionario>  Consultar()
        {
            sql = "SELECT * FROM Funcionario";

            //lista de funcionarios cadastrados
            List<Funcionario> funcionarios = new List<Funcionario>();

            try
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand(sql, connection);

                SqlDataReader leitor = cmd.ExecuteReader();

                while (leitor.Read())
                {
                    Funcionario funcionario = new Funcionario();
                    funcionario.ID = (long) leitor["IDFuncionario"];
                    funcionario.Nome = leitor["Nome"].ToString();
                    funcionario.RG = leitor["Rg"].ToString();
                    funcionario.CPF = leitor["Cpf"].ToString();
                    funcionario.Email = leitor["Email"].ToString();
                    funcionario.Telefone = leitor["Telefone"].ToString();

                    //adciona funcionario na lista de funcionarios

                    funcionarios.Add(funcionario);

                }
            }
            catch (SqlException ex)
            {

                msg = "Erro ao consultar funcionários: " + ex.Message;
                titulo = "Erro ...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            finally
            {
                connection.Close();
            }
            return funcionarios;
        }



        public Funcionario Consultar(string parametro)
        {
            sql = "SELECT * FROM Funcionario WHERE Cpf=@Cpf";
            Funcionario funcionario = new Funcionario();

            try
            {
                //abre a conexão 
                connection.Open();


                //comando sql

                SqlCommand cmd = new SqlCommand(sql, connection);

                //parametro do comando sql
                cmd.Parameters.AddWithValue("@Cpf", parametro);

               
                //leitor de dador sql
                //recebe os dados do banco
                SqlDataReader leitor = cmd.ExecuteReader();

                while (leitor.Read())
                {
                    //compara se o cpf dos registros é igual ao cpf de parâmetro
                    if (parametro.Equals(leitor["Cpf"].ToString()))
                    {
                        //cria o funcionario
                        funcionario = new Funcionario();
                        funcionario.ID = (long)leitor["IDFuncionario"];
                        funcionario.Nome = leitor["Nome"].ToString();
                        funcionario.RG = leitor["Rg"].ToString();
                        funcionario.CPF = leitor["Cpf"].ToString();
                        funcionario.Email = leitor["Email"].ToString();
                        funcionario.Telefone = leitor["Telefone"].ToString();

                    } //fim do if
                }// fim do while
               
            }//fim do try
            catch (SqlException ex)
            {

                msg = "Erro ao consultar o funcionário " + ex.Message;
                titulo = "Erro...";

                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();

            }
            return funcionario;

        }



        public void Excluir(Funcionario funcionario)

        {
            //instrução sql

            sql = "DELETE FROM Funcionario where IDFuncionario = @IDFuncionario" ;

            try
            {
                //abre a conexão com o banco de dados
                connection.Open();

                //cria um comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);


                //adiciona valor ao parâmetro @IDFuncionario
                cmd.Parameters.AddWithValue("@IDFuncionario", funcionario.ID);

                //executa comando Sql o banco de dados
                cmd.ExecuteNonQuery();

                //mensagem de feedback
                msg = "Funcionário" + funcionario.Nome + "excluído com sucesso!";

                //titulo da mensagem de feedback
                titulo = "sucesso ...";


                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (SqlException ex)
            {
                //mensagem
                msg = "Erro ao excluir o funcionário!" + ex.Message;

                //titulo
                titulo = "Erro..";

                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


            finally
            {
                connection.Close();
            }
        }

        public void Salvar(Funcionario funcionario)
        {
            //verifica se o id do funcionario é diferente de 0

            if(funcionario.ID != 0)
            {
                //update
                sql = "UPDATE Funcionario SET Nome=@Nome, Cpf=@Cpf, Rg=@Rg, Email=@Email, Telefone=@Telefone WHERE IDFuncionario=@IDFuncionario";
            }

            else
            {
                //insert

                sql = "INSERT INTO Funcionario (Nome, Cpf, Rg, Email, Telefone) VALUES (@Nome, @Cpf, @RG, @Email, @Telefone)";
            }


            try
            {
                

                //abre conexão com o banco
                connection.Open();

                //comando SQL

                SqlCommand cmd = new SqlCommand(sql, connection);

                cmd.Parameters.AddWithValue("@IDFuncionario", funcionario.ID);
                cmd.Parameters.AddWithValue("@Nome", funcionario.Nome);
                cmd.Parameters.AddWithValue("@Cpf", funcionario.CPF);
                cmd.Parameters.AddWithValue("@Rg", funcionario.RG);
                cmd.Parameters.AddWithValue("@Email", funcionario.Email);
                cmd.Parameters.AddWithValue("Telefone", funcionario.Telefone);

                //executa o INSERT
                cmd.ExecuteNonQuery();
                msg = "Funcionario " + funcionario.Nome + " salvo com sucesso!";
                titulo = "Sucesso...";

                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Information);

            }


            catch (SqlException ex)
            {
                msg = "Erro ao salvar funcionário!" + ex.Message;
                titulo = "Erro...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            finally
            {
                connection.Close();

            }
        }
            
        
    }
}
