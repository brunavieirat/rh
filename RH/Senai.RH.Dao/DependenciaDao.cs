﻿using RH.Senai.RH.Forms;
using RH.Senai.RH.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RH.Senai.RH.Dao
{
    class DependenciaDao : IDao<Dependencia>
    {
        //atributos
        //con~xão com o bd
        private SqlConnection connection;

        //instrução sql
        private string sql = null;

        //mensagem do messagebox
        private string msg = null;

        //titulo do messagebox
        private string titulo = null;
        

        public DependenciaDao()
        {
            connection = new ConnectionFactory().GetConnection();
        }

        public List<Dependencia> Consultar()
        {
            sql = "SELECT * FROM Dependencia";


            List<Dependencia> dependencias = new List<Dependencia>();

            try
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand(sql, connection);

                SqlDataReader leitor = cmd.ExecuteReader();

                while (leitor.Read())
                {
                    Dependencia dependencia = new Dependencia();
                    dependencia.ID = (long)leitor["IDDependencia"];
                    dependencia.Descricao = leitor["Descricao"].ToString();

                    dependencias.Add(dependencia);

                }



            }
            catch (SqlException ex)
            {
                msg = "Erro ao consultar. " + ex.Message;
                titulo = "Erro...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            finally
            {
                connection.Close();
            }
            return dependencias;
        }

        public Dependencia Consultar(string parametro)
        {
            throw new NotImplementedException();
        }

        public void Excluir(Dependencia dependencia)
        {

            sql = "DELETE FROM Dependencia Where IDDependencia=@IDDependencia";

            try
            {

                connection.Open();
                SqlCommand cmd = new SqlCommand(sql, connection);


                

                cmd.Parameters.AddWithValue("@IDDependencia", dependencia.ID);

                cmd.ExecuteNonQuery();


                msg = "Dependência exluída";

                titulo = "Sucesso";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
            catch (SqlException ex)
            {
                msg = "Erro na exclusão" + ex.Message;
                titulo = "Erro...";

                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            finally
            {

                connection.Close();
            }


            
        }

        public void Salvar(Dependencia dependencia)

        {
           
                if (dependencia.ID != 0)
                {
                    sql = "UPDATE Dependencia SET Descricao=@Descricao";
                }

                else {

                    sql = "INSERT INTO Dependencia (Descricao) VALUES(@Descricao)";
                }

            try
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand(sql, connection);

                cmd.Parameters.AddWithValue("@Descricao", dependencia.Descricao);
                cmd.ExecuteNonQuery();

                msg = "Dependência salva com sucesso!";
                titulo = "Sucesso!";

                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

            catch (SqlException ex)
            {
                msg = "Erro ao cadastrar dependência" + ex.Message;
                titulo = "Erro";

                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            finally
            {
                connection.Close();
            }




        }
    }
}
