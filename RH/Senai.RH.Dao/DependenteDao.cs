﻿using RH.Senai.RH.Model;
using Senai.RH.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RH.Senai.RH.Dao
{
    class DependenteDao : IDao<Dependente>
    {
        //variáveis do Dao
        // conexão com o banco de dados 
        private SqlConnection connection;

        // instrução sql
        private string sql = null;

        string msg = null;

        string titulo = null;


        //construtor
        public DependenteDao()
        {
            //conecta a aplicação ao banco de dados

            connection = new ConnectionFactory().GetConnection();
        }

        public List<Dependente> Consultar()
        {
            //instrução sql
            sql = "SELECT * FROM DependenteView";

            //lista de dependentes
            List<Dependente> dependentes = new List<Dependente>();

            //vdm

            try
            {
                //abre uma conexão com o banco de dados
                connection.Open();

                //cria um comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);

                // cria um leitor para receber os dados da tabela
                SqlDataReader leitor = cmd.ExecuteReader();

                //enquanto o leitor estiver lendo

                while (leitor.Read())
                {
                    // cria uma instancia de dependencia
                    Dependencia dependencia = new Dependencia();

                    //define os dados da dependencia
                    dependencia.ID = (long)leitor["IDDependencia"];
                    dependencia.Descricao = leitor["Descricao"].ToString();

                    //cria instancia de funcionario
                    Funcionario funcionario = new Funcionario();

                    //define os dados do funcionario
                    funcionario.ID = (long)leitor["IDFuncionario"];
                    funcionario.Nome = leitor["NomeFuncionario"].ToString();
                    funcionario.CPF = leitor["CPFFuncionario"].ToString();
                    funcionario.RG = leitor["Rg"].ToString();
                    funcionario.Email = leitor["Email"].ToString();
                    funcionario.Telefone = leitor["Telefone"].ToString();


                    Dependente dependente = new Dependente();

                    //define os dados do dependente
                    dependente.ID = (long)leitor["IDDependente"];
                    dependente.Nome = leitor["NomeDependente"].ToString();
                    dependente.CPF = leitor["CPFDependente"].ToString();
                    dependente.DataNascimento = (DateTime)leitor["DataNascimento"];
                    dependente.Dependencia = dependencia;
                    dependente.Funcionario = funcionario;

                    dependentes.Add(dependente);
                }

            }
            catch (SqlException ex)
            {

                msg = "Erro ao consultar os dependentes \n " + ex.Message;
                titulo = "Erro";

                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();

            }

            return dependentes;                                                                                                                                                                                                                                   

        }

        public Dependente Consultar(string parametro)
        {
            throw new NotImplementedException();
        }

        public void Excluir(Dependente t)
        {
            throw new NotImplementedException();
        }

        public void Salvar(Dependente dependente)
        {
            sql = "INSERT INTO Dependente (Nome, Cpf, DataNascimento, IDFuncionario, IDDependencia) VALUES (@Nome, @Cpf, @DataNascimento, @IDFuncionario, @IDDependencia)";

            try
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.Parameters.AddWithValue("@Nome", dependente.Nome);
                cmd.Parameters.AddWithValue("@Cpf", dependente.CPF);
                cmd.Parameters.AddWithValue("@DataNascimento", dependente.DataNascimento);
                cmd.Parameters.AddWithValue ("@IDFuncionario", dependente.Funcionario.ID);
                cmd.Parameters.AddWithValue("@IDDependencia", dependente.Dependencia.ID);

                cmd.ExecuteNonQuery();

                msg = "Dependente salvo com sucesso";
                titulo = "Sucesso ...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (SqlException ex)
            {

                msg="Erro"+ ex.Message;
                titulo = "Erro";

                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
