﻿using RH.Senai.RH.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RH
{
    public partial class HomeForm : Form
    {
        public HomeForm()
        {
            InitializeComponent();
        }

       

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

      
      
        private void dependênciaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DependenciaForm form = new DependenciaForm();
            form.ShowDialog();
        }

       
        private void dependenteToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            DependenteForm form = new DependenteForm();
            form.ShowDialog();
        }

        private void funcionárioToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            FuncionarioForm form = new FuncionarioForm();
            form.ShowDialog();

        }
    }
}
