﻿using RH.Senai.RH.Dao;
using RH.Senai.RH.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RH.Senai.RH.Forms
{
    public partial class DependenciaForm : Form
    {
        public DependenciaForm()
        {
            InitializeComponent();
        }

        private void DependenciaForm_Load(object sender, EventArgs e)
        {
            PreencheDados();
            LimparFormulario();
        }

        private void PreencheDados()
        {
            DependenciaDao dao = new DependenciaDao();

            dgvDependencia.DataSource = dao.Consultar();

            dgvDependencia.ClearSelection();

        }

        private void LimparFormulario()
        {
            txtDescricaoDependencia.Clear();
            txtIDDependencia.Clear();

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            //validação
            //verifica se os campos obrigatorios foram preenchidos

            if (string.IsNullOrEmpty(txtDescricaoDependencia.Text))
            {

                MessageBox.Show("Preencha todos os campos!");
            }
            else
            {
                //DependenciaDao dao = new DependenciaDao();
                Dependencia dependencia = new Dependencia();

                //obtem o id do funcionario
                //se o txt de id não estiver vazio

                if (!string.IsNullOrEmpty(txtIDDependencia.Text))
                {
                    //cria um id
                    long id = 0;

                    if (long.TryParse(txtIDDependencia.Text, out id))
                    {
                        //atribui o id ao objeto funcionario
                        dependencia.ID = id;

                    }


                }


                // atribui dados ao funcionario
                dependencia.Descricao = txtDescricaoDependencia.Text;

                // instancia o dao de funcionarios
                DependenciaDao dao = new DependenciaDao();


                //salva o funcionario no banco
                dao.Salvar(dependencia);


                LimparFormulario();
                PreencheDados();


            
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {


            if (string.IsNullOrEmpty(txtIDDependencia.Text))
            {
                string msg = "Selecione um funcionário na lista abaixo!";
                string titulo = "Operações não realizada...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                Dependencia dependencia = new Dependencia();

                long id = 0;
                if (long.TryParse(txtIDDependencia.Text, out id))
                {
                    dependencia.ID = id;
                }

                DialogResult resposta = MessageBox.Show("Deseja realmente excluir este funcionário? ", "Mensagem...", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                DependenciaDao dao = new DependenciaDao();

                if (resposta.Equals(DialogResult.Yes))
                {
                    dao.Excluir(dependencia);
                    PreencheDados();
                    LimparFormulario();
                }
            }
        }

        private void dgvDependencia_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvDependencia.CurrentRow != null)
            {
                //pega o id e coloca no textbox de id
                txtIDDependencia.Text = dgvDependencia.CurrentRow.Cells[0].Value.ToString();
                txtDescricaoDependencia.Text = dgvDependencia.CurrentRow.Cells[1].Value.ToString();
                

            }


        }

        private void dgvDependencia_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}