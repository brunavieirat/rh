﻿using RH.Senai.RH.Dao;
using Senai.RH.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RH.Senai.RH.Forms
{
    public partial class FuncionarioForm : Form
    {
        public FuncionarioForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("CPF: " + txtCpfFuncionario.Text);
        }

        private void FuncionarioForm_Load(object sender, EventArgs e)
        {
            
            PreencheDados();
           
                        
        }
        /// <summary>
        ///MOstra blabla bla
        /// </summary>
        private void PreencheDados()
        {

            //instancia a dao
            FuncionarioDao dao = new FuncionarioDao();

            //preenche o data grid view;
            dgvFuncionarios.DataSource = dao.Consultar();
            //dgvFuncionarios.DataSource = new FuncionarioDao().Consultar();

            //oculta algumas colunas
            dgvFuncionarios.Columns["ID"].Visible = false;
            dgvFuncionarios.Columns["Rg"].Visible = false;

            //limpa seleção 
            dgvFuncionarios.ClearSelection();

            //limpar campos dos formulario
            LimparFormulario();
        }

        private void btnSalvarFuncionario_Click(object sender, EventArgs e)
        {
            //validação
            //verifica se os campos obrigatorios foram preenchidos

            if (string.IsNullOrEmpty(txtNomeFuncionario.Text) && string.IsNullOrEmpty(txtCpfFuncionario.Text) && string.IsNullOrEmpty(txtRgFuncionario.Text)
                && string.IsNullOrEmpty(txtEmailFuncionario.Text) && string.IsNullOrEmpty(txtTelFuncionario.Text))
            {
                MessageBox.Show("Preencha todos os campos!");
            }
            else
            {


                //instancia um funcionário
                Funcionario funcionario = new Funcionario();

                //obtem o id do funcionario
                //se o txt de id não estiver vazio

                if (!string.IsNullOrEmpty(txtIDFuncionario.Text))
                {
                    //cria um id
                    long id = 0;
                    if (long.TryParse(txtIDFuncionario.Text, out id))
                    {
                        //atribui o id ao objeto funcionario
                        funcionario.ID = id;
                    }


                }


                // atribui dados ao funcionario
                funcionario.Nome = txtNomeFuncionario.Text;
                funcionario.RG = txtRgFuncionario.Text;
                funcionario.CPF = txtCpfFuncionario.Text;
                funcionario.Email = txtEmailFuncionario.Text;
                funcionario.Telefone = txtTelFuncionario.Text;

                // instancia o dao de funcionarios
                FuncionarioDao funcdao = new FuncionarioDao();

                //salva o funcionario no banco
                funcdao.Salvar(funcionario);

                LimparFormulario();
                PreencheDados();


            }            
        }


        public void LimparFormulario() {

            txtIDFuncionario.Clear();
            txtCpfFuncionario.Clear();
            txtEmailFuncionario.Clear();
            txtNomeFuncionario.Clear();
            txtRgFuncionario.Clear();
            txtTelFuncionario.Clear();
            txtCpfFuncionario.Focus();

        }

        private void dgvFuncionarios_SelectionChanged(object sender, EventArgs e)
        {
            // se alguma linha for selecionada
            if(dgvFuncionarios.CurrentRow != null)
            {
                //pega o id e coloca no textbox de id

                txtIDFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[0].Value.ToString();
                txtNomeFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[1].Value.ToString();
                txtRgFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[2].Value.ToString();
                txtCpfFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[3].Value.ToString();               
                txtEmailFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[4].Value.ToString();
                txtTelFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[5].Value.ToString();
                

            }

        }

        private void btnExcluirFuncionario_Click(object sender, EventArgs e)
        {
            //verifica se o textbox de ID é nulo ou vazio 
            //se sim nenhum funcionario foi selecionado na lista

            if (string.IsNullOrEmpty(txtIDFuncionario.Text))
            {
                string msg = "Selecione um funcionário na lista abaixo!";
                string titulo = "Operações não realizada...";
                MessageBox.Show(msg,titulo, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                //instancia um funcionario
                Funcionario funcionario = new Funcionario();

                //cria um id para o funcionario
                long id = 0;

                if(long.TryParse(txtIDFuncionario.Text, out id))
                {

                    funcionario.ID = id;

                }

                FuncionarioDao dao = new FuncionarioDao();

                DialogResult resposta = MessageBox.Show("Deseja realmente excluir este funcionário? ", "Mensagem...", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (resposta.Equals(DialogResult.Yes))
                {
                    //exclui o funcionario
                    dao.Excluir(funcionario);

                    //atualiza o datagrid

                    PreencheDados();

                }


                



            }

        }

       

        private void txtCpfFuncionario_Leave(object sender, EventArgs e)
        {
            //armazena o texto do textbox de cpf
            string cpf = txtCpfFuncionario.Text;

            //cria um funcionario
            Funcionario funcionario = new FuncionarioDao().Consultar(cpf);

            //verifica SE funcionario não é null
            if (funcionario !=null)
            {
                txtIDFuncionario.Text = funcionario.ID.ToString();
                txtNomeFuncionario.Text = funcionario.Nome;
                txtRgFuncionario.Text = funcionario.RG;
                txtCpfFuncionario.Text = funcionario.CPF;
                txtEmailFuncionario.Text = funcionario.Email;
                txtTelFuncionario.Text = funcionario.Telefone;

                
            }


        }

        private void dgvFuncionarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
