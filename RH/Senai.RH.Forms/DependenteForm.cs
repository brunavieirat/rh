﻿using RH.Senai.RH.Dao;
using RH.Senai.RH.Model;
using Senai.RH.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RH.Senai.RH.Forms
{
    public partial class DependenteForm : Form
    {
        public DependenteForm()
        {
            InitializeComponent();
        }

        private void DependenteForm_Load(object sender, EventArgs e)
        {
            PreencheComboBox();
            //Preenche o data grid view
            PreencheDados();
        }


        private void PreencheComboBox()
        {

            //preenche a combobox
            //define a fonte de dados da combobox
            cboDependencia.DataSource = new DependenciaDao().Consultar();

            //define o que mostrar na combobox
            cboDependencia.DisplayMember = "Descricao";

            cboFuncionario.DataSource = new FuncionarioDao().Consultar();
            cboFuncionario.DisplayMember = "Nome";
        }

        private void PreencheDados()
        {
            //define a fonte de dados do data grid view
            dgvDependentes.DataSource = new DependenteDao().Consultar();


        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            Dependente dependente = new Dependente();

            dependente.Nome = txtNomeDependente.Text;
            dependente.CPF = txtCPFDependente.Text;


            //pega a data do data time picker
            dependente.DataNascimento = dtpDataNascimento.Value;

            //obtem dependencia do combobox
            Dependencia dependencia = (Dependencia)cboDependencia.SelectedItem;

            Funcionario funcionario = (Funcionario)cboFuncionario.SelectedItem;

            //associa o dependente a dependencia
            dependente.Dependencia = dependencia;

            //associa o depente ao funcionario
            dependente.Funcionario = funcionario;


            DependenteDao dao = new DependenteDao();

            dao.Salvar(dependente);


            PreencheDados();

           
        }
        private void  LimparFormulario()
        {
            txtIDDependente.Clear();
            txtCPFDependente.Clear();
            dtpDataNascimento.Value = DateTime.Now;
            txtNomeDependente.Clear();
            PreencheComboBox();
            txtCPFDependente.Focus();

        }

        private void dgvDependentes_SelectionChanged(object sender, EventArgs e)
        {
            //preenche os campos do form

            txtIDDependente.Text = dgvDependentes.CurrentRow.Cells[0].Value.ToString();
            txtNomeDependente.Text = dgvDependentes.CurrentRow.Cells[1].Value.ToString();
            txtCPFDependente.Text = dgvDependentes.CurrentRow.Cells[2].Value.ToString();
            dtpDataNascimento.Value = (DateTime) dgvDependentes.CurrentRow.Cells[3].Value;
            cboDependencia.Text = dgvDependentes.CurrentRow.Cells[4].Value.ToString();
            cboFuncionario.Text = dgvDependentes.CurrentRow.Cells[5].Value.ToString();
        }
    }
}
