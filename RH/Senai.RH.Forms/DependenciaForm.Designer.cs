﻿namespace RH.Senai.RH.Forms
{
    partial class DependenciaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblID = new System.Windows.Forms.Label();
            this.txtIDDependencia = new System.Windows.Forms.TextBox();
            this.txtDescricaoDependencia = new System.Windows.Forms.TextBox();
            this.lblDesc = new System.Windows.Forms.Label();
            this.dgvDependencia = new System.Windows.Forms.DataGridView();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDependencia)).BeginInit();
            this.SuspendLayout();
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Location = new System.Drawing.Point(29, 29);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(18, 13);
            this.lblID.TabIndex = 0;
            this.lblID.Text = "ID";
            // 
            // txtIDDependencia
            // 
            this.txtIDDependencia.Enabled = false;
            this.txtIDDependencia.Location = new System.Drawing.Point(32, 45);
            this.txtIDDependencia.Name = "txtIDDependencia";
            this.txtIDDependencia.Size = new System.Drawing.Size(100, 20);
            this.txtIDDependencia.TabIndex = 1;
            // 
            // txtDescricaoDependencia
            // 
            this.txtDescricaoDependencia.Location = new System.Drawing.Point(32, 91);
            this.txtDescricaoDependencia.Name = "txtDescricaoDependencia";
            this.txtDescricaoDependencia.Size = new System.Drawing.Size(244, 20);
            this.txtDescricaoDependencia.TabIndex = 2;
            // 
            // lblDesc
            // 
            this.lblDesc.AutoSize = true;
            this.lblDesc.Location = new System.Drawing.Point(29, 75);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(55, 13);
            this.lblDesc.TabIndex = 3;
            this.lblDesc.Text = "Descrição";
            // 
            // dgvDependencia
            // 
            this.dgvDependencia.AllowUserToAddRows = false;
            this.dgvDependencia.AllowUserToDeleteRows = false;
            this.dgvDependencia.AllowUserToOrderColumns = true;
            this.dgvDependencia.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDependencia.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvDependencia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDependencia.Location = new System.Drawing.Point(32, 176);
            this.dgvDependencia.Name = "dgvDependencia";
            this.dgvDependencia.ReadOnly = true;
            this.dgvDependencia.RowHeadersVisible = false;
            this.dgvDependencia.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvDependencia.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDependencia.Size = new System.Drawing.Size(244, 169);
            this.dgvDependencia.TabIndex = 4;
            this.dgvDependencia.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDependencia_CellContentClick);
            this.dgvDependencia.SelectionChanged += new System.EventHandler(this.dgvDependencia_SelectionChanged);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(201, 133);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 5;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(120, 133);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExcluir.TabIndex = 6;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // DependenciaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(311, 374);
            this.Controls.Add(this.btnExcluir);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.dgvDependencia);
            this.Controls.Add(this.lblDesc);
            this.Controls.Add(this.txtDescricaoDependencia);
            this.Controls.Add(this.txtIDDependencia);
            this.Controls.Add(this.lblID);
            this.Name = "DependenciaForm";
            this.Text = "Dependencia";
            this.Load += new System.EventHandler(this.DependenciaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDependencia)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.TextBox txtIDDependencia;
        private System.Windows.Forms.TextBox txtDescricaoDependencia;
        private System.Windows.Forms.Label lblDesc;
        private System.Windows.Forms.DataGridView dgvDependencia;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Button btnExcluir;
    }
}