﻿namespace RH.Senai.RH.Forms
{
    partial class DependenteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtIDDependente = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Nome = new System.Windows.Forms.Label();
            this.txtNomeDependente = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCPFDependente = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dtpDataNascimento = new System.Windows.Forms.DateTimePicker();
            this.cboFuncionario = new System.Windows.Forms.ComboBox();
            this.cboDependencia = new System.Windows.Forms.ComboBox();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.dgvDependentes = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDependentes)).BeginInit();
            this.SuspendLayout();
            // 
            // txtIDDependente
            // 
            this.txtIDDependente.Enabled = false;
            this.txtIDDependente.Location = new System.Drawing.Point(71, 47);
            this.txtIDDependente.Name = "txtIDDependente";
            this.txtIDDependente.Size = new System.Drawing.Size(100, 20);
            this.txtIDDependente.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(68, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "ID";
            // 
            // Nome
            // 
            this.Nome.AutoSize = true;
            this.Nome.Location = new System.Drawing.Point(68, 70);
            this.Nome.Name = "Nome";
            this.Nome.Size = new System.Drawing.Size(35, 13);
            this.Nome.TabIndex = 3;
            this.Nome.Text = "Nome";
            // 
            // txtNomeDependente
            // 
            this.txtNomeDependente.Location = new System.Drawing.Point(71, 86);
            this.txtNomeDependente.Name = "txtNomeDependente";
            this.txtNomeDependente.Size = new System.Drawing.Size(337, 20);
            this.txtNomeDependente.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(185, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "CPF";
            // 
            // txtCPFDependente
            // 
            this.txtCPFDependente.Location = new System.Drawing.Point(188, 47);
            this.txtCPFDependente.Mask = "000.000.000-00";
            this.txtCPFDependente.Name = "txtCPFDependente";
            this.txtCPFDependente.Size = new System.Drawing.Size(100, 20);
            this.txtCPFDependente.TabIndex = 6;
            this.txtCPFDependente.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(304, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Data de Nascimento";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(428, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Dependência";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(70, 113);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Funcionário";
            // 
            // dtpDataNascimento
            // 
            this.dtpDataNascimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDataNascimento.Location = new System.Drawing.Point(307, 47);
            this.dtpDataNascimento.Name = "dtpDataNascimento";
            this.dtpDataNascimento.Size = new System.Drawing.Size(101, 20);
            this.dtpDataNascimento.TabIndex = 12;
            // 
            // cboFuncionario
            // 
            this.cboFuncionario.FormattingEnabled = true;
            this.cboFuncionario.Location = new System.Drawing.Point(73, 132);
            this.cboFuncionario.Name = "cboFuncionario";
            this.cboFuncionario.Size = new System.Drawing.Size(335, 21);
            this.cboFuncionario.TabIndex = 13;
            // 
            // cboDependencia
            // 
            this.cboDependencia.FormattingEnabled = true;
            this.cboDependencia.Location = new System.Drawing.Point(430, 132);
            this.cboDependencia.Name = "cboDependencia";
            this.cboDependencia.Size = new System.Drawing.Size(224, 21);
            this.cboDependencia.TabIndex = 14;
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(498, 171);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 15;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(579, 171);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExcluir.TabIndex = 16;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.UseVisualStyleBackColor = true;
            // 
            // dgvDependentes
            // 
            this.dgvDependentes.AllowUserToAddRows = false;
            this.dgvDependentes.AllowUserToDeleteRows = false;
            this.dgvDependentes.AllowUserToResizeColumns = false;
            this.dgvDependentes.AllowUserToResizeRows = false;
            this.dgvDependentes.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvDependentes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDependentes.Location = new System.Drawing.Point(67, 218);
            this.dgvDependentes.MultiSelect = false;
            this.dgvDependentes.Name = "dgvDependentes";
            this.dgvDependentes.ReadOnly = true;
            this.dgvDependentes.RowHeadersVisible = false;
            this.dgvDependentes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvDependentes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDependentes.Size = new System.Drawing.Size(587, 198);
            this.dgvDependentes.TabIndex = 17;
            this.dgvDependentes.SelectionChanged += new System.EventHandler(this.dgvDependentes_SelectionChanged);
            // 
            // DependenteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(722, 462);
            this.Controls.Add(this.dgvDependentes);
            this.Controls.Add(this.btnExcluir);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.cboDependencia);
            this.Controls.Add(this.cboFuncionario);
            this.Controls.Add(this.dtpDataNascimento);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtCPFDependente);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Nome);
            this.Controls.Add(this.txtNomeDependente);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtIDDependente);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DependenteForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DependenteForm";
            this.Load += new System.EventHandler(this.DependenteForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDependentes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtIDDependente;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Nome;
        private System.Windows.Forms.TextBox txtNomeDependente;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox txtCPFDependente;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtpDataNascimento;
        private System.Windows.Forms.ComboBox cboFuncionario;
        private System.Windows.Forms.ComboBox cboDependencia;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.DataGridView dgvDependentes;
    }
}